<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawab extends Model
{
    use HasFactory;

    protected $table = 'jawab';

    protected $fillable = ['user_id', 'tanya_id', 'jawaban'];

    public function pertanyaan()
    {
        return $this->belongsTo(Pertanyaan::class, 'tanya_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
