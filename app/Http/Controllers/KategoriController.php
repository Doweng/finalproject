<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = DB::table('kategori')->get();

        return view('kategori.index', ['kategori' => $kategori]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',      //name,age,bio sesuaikn dengan yg di tabel DB Cast
        ],
        [
            'name.required' => 'cast name harus di isi tidak boleh kosong'
        ]);

        DB::table('kategori')->insert([
            'name' => $request['name'],
        ]);

        return redirect('/kategori');  //untuk melempar ke halaman lain sesuai url yg di tuju get(/kategori)
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = Kategori::find($id);

        return view('kategori.detail', ['kategori' => $kategori]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);

        return view('kategori.edit', ['kategori' => $kategori]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',      //name,age,bio sesuaikn dengan yg di tabel DB Cast
        ],
        [
            'name.required' => 'cast name harus di isi tidak boleh kosong'
        ]);

        DB::table('kategori')
            ->where('id', $id)
            ->update([
                'name' => $request['name']
            ]);

            return redirect('/kategori');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       DB::table('kategori')->where('id', $id)->delete();

       return redirect('/kategori');
    }
}
