<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use App\Models\User;
use File;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::all();
        $tanya = Pertanyaan::all();

        return view('pertanyaan.index', compact('kategori', 'tanya'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();

        return view('pertanyaan.tambah', ['kategori' => $kategori]);
        // return view ('pertanyaan.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg',
            'kategori_id' => 'required',
            // 'user_id' => 'required',
        ]);

        $id = Auth::id();

        $gambarName = time().'.'.$request->gambar->extension();   //time() agar file unique berdasarkan wktu uploadnya
   
        $request->gambar->move(public_path('image'), $gambarName);

        //cara untuk save dataNya
        $tanya = new Pertanyaan;

        $tanya->pertanyaan = $request->pertanyaan;
        $tanya->gambar = $gambarName;
        $tanya->kategori_id = $request->kategori_id;
        $tanya->user_id = $id;

        $tanya->save();

        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tanya = Pertanyaan::findOrFail($id);

        return view ('pertanyaan.detail', ['tanya' => $tanya]);

        // Jika ingin menggunakan find
        //  $tanya = Pertanyaan::find($id)
        // if (!tanya){
        //     return redirect ('/pertanyaan');
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::all();

        $tanya = Pertanyaan::findOrFail($id);

        return view ('pertanyaan.edit', ['tanya' => $tanya, 'kategori'=> $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg',
            'kategori_id' => 'required',
            // 'user_id' => 'required',
        ]);

        $tanya = Pertanyaan::findOrFail($id);

        $tanya->pertanyaan = $request->pertanyaan;
        $tanya->kategori_id = $request->kategori_id;
        $tanya->user_id = $id;

        if ($request->has('gambar')) {
            $path = 'image/';                     //fungsi untuk hapus data image di folder public
            File::delete($path. $tanya->gambar);

            $Newgambar = time().'.'.$request->gambar->extension();   //time() agar file unique berdasarkan wktu uploadnya
   
            $request->gambar->move(public_path('image'), $Newgambar);

            $tanya->gambar = $Newgambar;  //uutk update data foto
        }

        $tanya->save();

        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tanya = Pertanyaan::find($id);

        $path = 'image/';                     //fungsi untuk hapus data image di folder public
        File::delete($path. $tanya->gambar);

        $tanya->delete();

        return redirect('/pertanyaan');
    }
}
