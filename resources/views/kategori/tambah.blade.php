@extends('templates.main')

@section('judul')
    Tambah Kategori
@endsection

@section('content')
<form action="/kategori" method="POST">
    @csrf
        <div class="form-group my-3">
            <label>Kategori Name</label>
            <input type="text" placeholder="Masukan kategori name" class="form-control @error('name') is-invalid @enderror" name="name" >
        </div>
        {{-- @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="/kategori" class="btn btn-secondary">Kembali</a>
      </form>
@endsection