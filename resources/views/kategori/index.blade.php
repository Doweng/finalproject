@extends('templates.main')

@section('judul')
    Kategori
@endsection

@section('content')
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-md-row mb-3">
          <li class="nav-item">
            <a class="nav-link active" href="/kategori/create"><i class="bx bx-plus my-1"></i> Tambah</a>
          </li>
        </ul>
        <div class="card mb-4">
          <h5 class="card-header">List Kategori</h5>
          
          <hr class="my-0" />
          <div class="card-body">
            <div class="table-responsive pt-0">
              <table class="table table-bordered table-hover table-sm" id="example" style="width: 100%">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Category Name</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($kategori as $key => $item)
                      <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->name }}</td>
                        <td>
                          <form action="/kategori/{{ $item->id }}" method="POST">
                              @method('delete')
                              @csrf
                              <a href="/kategori/{{ $item->id }}" class="btn btn-success btn-sm me-2">Detail</a>
                              <a href="/kategori/{{ $item->id }}/edit" class="btn btn-info btn-sm me-2" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"><i class='bx bx-edit'></i></a>
                              <button type="submit" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete" onclick="return confirm('Yakin ingin hapus data?');"><i class='bx bxs-trash'></i>
                              
                          </form>
                      </td>
                      </tr>
                  @empty
                      <tr>
                        <td colspan="3" class="text-center">Tidak ada data!</td>
                      </tr>
                  @endforelse
                </tbody>
             </table>

            </div>
            

          </div>
        </div>
      </div>
    </div>
@endsection