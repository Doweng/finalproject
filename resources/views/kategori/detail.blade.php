@extends('templates.main')

@section('judul')
    Kategori
@endsection

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="card mb-4">
          <h5 class="card-header">Detail Kategori</h5>
          
          <hr class="my-0" />
          <div class="card-body">

            <table class="table table-dark">
                <thead>
                    <h2>Tema: {{ $kategori->name }}</h2>

                    <hr>
                    <div class="row">
                      @forelse ($kategori->pertanyaan as $item)
                      <div class="col-4">
                        <div class="card my-3">
                            <img src="{{ asset('image/'.$item->gambar) }}" class="card-img-top" alt="..." height="200" width="200px">
                            <div class="card-body">
                              {{-- <h5 class="card-title">{{ $item->pertanyaan }}</h5> --}}
                              <h5>
                                <p class="card-text">{{ Str::limit($item->pertanyaan, 20) }}</p>
                              </h5>
                              <a href="/pertanyaan/{{ $item->id }}" class="btn btn-primary">Detail</a>
                            </div>
                        </div>
                    </div>
                      @empty
                          <h2>Belum ada pertanyaan</h2>
                      @endforelse
                    </div>

                    <a href="/kategori" class="btn btn-secondary btn-sm">Back</a>
                </thead> 
            </table>

          </div>
        </div>
      </div>
    </div>
@endsection