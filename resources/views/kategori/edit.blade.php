@extends('templates.main')

@section('judul')
    Halaman Edit Kategori
@endsection

@section('content')
<form action="/kategori/{{ $kategori->id }}" method="POST">
    @method('put')
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <h5 class="card-header">Edit Kategori</h5>
            
                <hr class="my-0" />
                <div class="card-body">
                    <label>Kategori Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ $kategori->name }}" name="name">
  
                    <button type="submit" class="btn btn-success my-3">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection