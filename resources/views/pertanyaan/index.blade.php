@extends('templates.main')

@section('judul')
    Pertanyaan
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-md-row mb-3">
            <li class="nav-item">
            <a class="nav-link active" href="/pertanyaan/create"><i class="bx bx-plus me-1"></i> Pertanyaan</a>
            </li>
        </ul>

        <div class="row">
            @forelse ($tanya as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{ asset('image/'.$item->gambar) }}" class="card-img-top" alt="..." height="200" width="200px">
                    <div class="card-body">
                      {{-- <h5 class="card-title">{{ $item->pertanyaan }}</h5> --}}
                      <h5>
                        <p class="card-text">{{ Str::limit($item->pertanyaan, 20) }}</p>
                      </h5>
                      <a href="/pertanyaan/{{ $item->id }}" class="btn btn-primary">{{ $item->kategori->name }}</a>
                    </div>
                </div>
            </div>
            @empty
                <h5>Tidak ada Pertanyaan</h5>
            @endforelse
        </div>
       
@endsection