@extends('templates.main')

@section('judul')
    Halaman Detail Pertanyaan
@endsection

@section('content')
<div class="row">
    <div class="col-9 strech-card">
        <div class="card">
            <div class="card-body">
              {{-- <h5 class="card-title">{{ $item->pertanyaan }}</h5> --}}
              <h5>
                <p class="card-text">{{ $tanya->pertanyaan, 20 }}</p>
              </h5>
              <img src="{{ asset('image/'.$tanya->gambar) }}" class="card-img-top" alt="..." width="100px">
              <div class="col my-3">
                <form action="/pertanyaan/{{ $tanya->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/pertanyaan/{{ $tanya->id }}/edit" class="btn btn-info btn-block">Edit</a>
                    <input type="submit" class="btn btn-danger btn-block" value="Delete">
                </form>
              </div>
              <a href="/pertanyaan" class="btn btn-secondary">Kembali</a>
              
            </div>
        </div>
    </div>
</div>
<hr>
<h3 class="m-4">Jawaban </h3>
@forelse ($tanya->jawaban as $item)
<div class="card-body">
    <p class="card-text"><small class="text-muted">{{$item->created_at->diffForHumans()}}</small></p>
    <p class="card-title d-inline-block">Di jawab oleh: {{$item->user->name}}</p>
    <h3>{{ $item->jawaban }}</h3>
</div>
@empty
    
@endforelse
<hr>
<h5 class="m-4">Beri Jawaban</h5>
    <form action="/jawaban" method="POST" enctype="multipart/form-data" class="m-4">
        @csrf
        <input type="hidden" value="{{ $tanya->id }}" name="tanya_id" >
        <div class="form-group my-3">
            <textarea type="text" class="form-control" name="jawaban" placeholder="Masukkan jawaban anda"></textarea>
            {{-- @error('jawaban')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror --}}
        </div>
        {{-- <div class="form-group my-3">
            <label for="title">Gambar</label>
            <input type="file" class="form-control" name="gambar">
        </div> --}}
            {{-- @error('gambar')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror --}}
        <button type="submit" class="btn btn-primary">Tambah Jawaban</button>
    </form>
@endsection
{{-- <div class="form-group my-3">
    <label>Beri Jawaban</label>
    <textarea type="text" placeholder="Masukan jawaban anda" class="form-control @error('jawab') is-invalid @enderror" name="jawab" ></textarea>
</div>
<div class="form-group">
    <label>Gambar</label>
    <input type="file" name="gambar" class="form-control @error('gambar') is-invalid @enderror">
</div> --}}