@extends('templates.main')

@section('judul')
    Pertanyaan Baru
@endsection

@section('content')
<form action="/pertanyaan" method="POST" enctype="multipart/form-data">
    @csrf
        <div class="form-group my-3">
            <label>Tema Pertanyaan</label>
            <textarea type="text" placeholder="Masukan pertanyaan anda" class="form-control @error('pertanyaan') is-invalid @enderror" name="pertanyaan" ></textarea>
        </div>

        <div class="form-group">
            <label>Gambar</label>
            <input type="file" name="gambar" class="form-control @error('gambar') is-invalid @enderror">
        </div>

        <div class="form-group">
            <label>Kategori</label>
            <select name="kategori_id" id="" class="form-control @error('kategori_id') is-invalid @enderror">
                <option value="">---Pilih Kategori---</option>
                @forelse ($kategori as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @empty
                    <option value="">Tidak Ada Kategori</option>
                @endforelse
            </select>
        </div>
        {{-- @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}
        <button type="submit" class="btn btn-success my-3">Simpan</button>
      </form>
@endsection