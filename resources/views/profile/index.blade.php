@extends('templates.main')

@section('judul')
    My Profile
@endsection

@section('content')
    <form method="POST" action="/profile/{{$detailProfile->id}}">
        @csrf
        @method('PUT')
        <div class="row mb-3">
            <label for="umur" class="col-md-4 col-form-label text-md-end">Umur</label>
            <div class="col-md-6">
                <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{$detailProfile->umur}}" autocomplete="name" autofocus>
            </div>
            @error('umur')
                <strong>{{ $message }}</strong>
            @enderror
        </div>
        <div class="row mb-3">
            <label class="col-md-4 col-form-label text-md-end">Alamat</label>
            <div class="col-md-6">
                <textarea name="alamat" class="form-control"> {{$detailProfile->alamat}}</textarea>
            </div>
        </div>
        @error('alamat')
                <strong>{{ $message }}</strong>
        @enderror

        <div class="row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Update') }}
                </button>
            </div>
        </div>
    </form>
                
@endsection