@extends('templates.main')

@section('judul')
    Update Profile
@endsection

@section('content')
<form action="/profil/{{ $profil->id }}" method="POST">
    @csrf
    @method('put')
    <h3>Nama User : {{ $profil->users->name }}</h3>
    <h3>Email User : {{ $profil->users->email }}</h3>
        <div class="form-group my-3">
            <label>Alamat</label>
            <textarea placeholder="Masukan alamat anda" class="form-control @error('alamat') is-invalid @enderror" name="alamat" >{{ $profil->alamat }}</textarea>
        </div>
        <div class="form-group my-3">
            <label>Umur</label>
            <input type="number" value="{{ $profil->umur }}" class="form-control @error('umur') is-invalid @enderror" name="umur" >
        </div>
        {{-- @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="/pertanyaan" class="btn btn-secondary">Kembali</a>
      </form>
@endsection