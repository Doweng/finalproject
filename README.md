## Informasi Kelompok

Kelompok 6

-   Ardhiansyah (@Doweng)
-   Lutfi Cahya Nugraha (@LutfiiCahya)
-   Widia Novita Sari (@widnvts)

Tema : Forum Tanya Jawab

## ERD

![forum-pertanyaan.png](forum-pertanyaan.png?raw=true)

## Link Demo

-   https://drive.google.com/file/d/1XB1csNv_4374FG0a6A27LVe8PjLNeeY1/view?usp=sharing

## Link Deploy

-   forumdiskusi.forumdiskusi2.sanbercodeapp.com
