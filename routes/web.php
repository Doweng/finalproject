<?php

use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\JawabanController;
use App\Http\Controllers\ProfilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });
});

// AUTH
Route::get('/login', function () {
    return view('auth.login');
});

// KATEGORI
Route::get('/kategori', function () {
    return view('kategori.index');
});

// PERTANYAAN
Route::get('/pertanyaan', function () {
    return view('pertanyaan.index');
});
// Route::get('/pertanyaan/create', [PertanyaanController::class, 'create']);


// JAWAB
Route::get('/jawab', function () {
    return view('jawab.index');
});

Route::resource('profile', ProfileController::class)->only(['index','update']);



Route::resource('kategori', KategoriController::class);

Route::resource('pertanyaan', PertanyaanController::class);
Route::resource('jawaban', JawabanController::class);
Route::resource('profil', ProfilController::class);


Auth::routes();
